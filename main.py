from prometheus_client import start_http_server, REGISTRY
from prometheus_client.core import GaugeMetricFamily
from vcgencmd import Vcgencmd
import time
import signal
import os

vcg = Vcgencmd()


def temperature_of_raspberry_pi():
    cpu = vcg.measure_temp()
    CustomCollector.cpu_temp = str(cpu)


class GracefulKiller:
    kill_now = False

    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_gracefully)
        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, *args):
        self.kill_now = True


class CustomCollector(object):
    cpu_temp = 1.0

    # host_name =

    def __init__(self):
        pass

    def collect(self):
        yield GaugeMetricFamily('pi_cpu_temperature', 'Temperature of Raspberry Pi reported by vcgencmd',
                                value=self.cpu_temp)


if __name__ == '__main__':
    # Start up the server to expose the metrics.
    start_http_server(8000)
    killer = GracefulKiller()
    REGISTRY.register(CustomCollector())
    while not killer.kill_now:
        temperature_of_raspberry_pi()
        time.sleep(3)

print("End of the program. I was killed gracefully :)")
