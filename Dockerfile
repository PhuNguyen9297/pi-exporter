FROM python:3.9.14-alpine3.15
RUN apk add --no-cache raspberrypi
ENV PATH="${PATH}:/opt/vc/bin"
WORKDIR app
COPY requirements.txt .
RUN pip install -r requirements.txt
COPY main.py .
ENTRYPOINT python main.py